# Clausewitz::Spelling

Tools for performing dialect/language-aware spellchecking of Paradox
Interactive's Clausewitz engine localisation files, used in games like Europa
Universalis and Hearts of Iron IV.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'clausewitz-spelling'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install clausewitz-spelling

## Usage

```
> clausewitz-spellcheck --help
Options:
  -c, --custom-wordlist=<s>     Text file containing newline-delimited list of
                                custom words
  -e, --english-dialect=<s>     Two-letter code indicating dialect of English
                                to use
  -s, --spanish-dialect=<s>     Two-letter code indicating dialect of Spanish
                                to use
  -u, --suggestion-count=<i>    How many suggestions to display
  -h, --help                    Show this message
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/owb-dev-team/clausewitz-spelling. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Clausewitz::Spelling project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/owb-dev-team/clausewitz-spelling/blob/master/CODE_OF_CONDUCT.md).
