#!/usr/bin/env ruby
require 'clausewitz/spelling/checker'
require 'optimist'

class Main
  def initialize(args)
    @opts, @args = parse_args(args)
  end

  def parse_args(args)
    opts = Optimist::options(args) do
      opt :custom_dicts,
        "List of custom dictionaries to load",
        type: :strings
      opt :custom_dict_root,
        "Directory containing per-language-dialect custom dicts",
        type: :string
      opt :suggestion_count,
        "How many suggestions to display",
        type: :int
      opt :verbose,
        "Whether to display additional output while running",
        default: false
      opt :commit_range,
        "Range of commits for limiting check to only changed lines",
        type: :string
      Clausewitz::Localisation::LANG_MAP.each do |_, config|
        opt "#{config.name}_dialect".to_sym,
          "Select dialect for #{config.name.capitalize}",
          type: :string
      end
    end

    dialect_map = {}
    dialect_opts = opts.keys.select { |k| k =~ /.+_dialect/ }
    dialect_opts.each do |dialect_opt_key|
      next unless opts[dialect_opt_key]
      next if dialect_opt_key.to_s.end_with?('given')
      language_name = dialect_opt_key[/(.+)_dialect/, 1]
      dialect_map[language_name] = opts[dialect_opt_key]
    end

    checker_opts = {}
    checker_opts[:custom_dict_root] = opts[:custom_dict_root]
    checker_opts[:custom_dicts]     = opts[:custom_dicts]
    checker_opts[:dialect_map]      = dialect_map
    checker_opts[:suggestion_count] = opts[:suggestion_count]
    checker_opts[:verbose]          = opts[:verbose]
    checker_opts[:commit_range]     = opts[:commit_range]

    [checker_opts, args]
  end

  def run
    spellchecker = Clausewitz::Spelling::Checker.new(@opts)
    results = @args.map { |arg| spellchecker.check_file(arg) }
    results.each { |r| puts r }
    failed = results.any?(&:failed?)
    if failed
      overall_failures = results.reduce(0) do |memo, obj|
        memo += obj.failure_total
      end
      puts "\nFound #{overall_failures} errors in total. :(".red
    else
      puts "\nAll entries checked with no errors! :)".green
    end
    !failed
  end
end

exit(Main.new(ARGV).run)

# vim: set ft=ruby ts=2 sw=2 tw=79 :
