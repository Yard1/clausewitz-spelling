require 'colorize'

module Clausewitz; module Spelling
  class FileResults
    def initialize(filepath, lang_results)
      @filepath     = filepath
      @lang_results = lang_results
    end

    def ignored?
      @lang_results.all?(&:ignored?)
    end

    def ignored
      @lang_results.select(&:ignored?)
    end

    def failed?
      @lang_results.any?(&:failed?)
    end

    def failures
      @lang_results.select(&:failed?)
    end

    def ignored_total
      @lang_results.reduce(0) { |memo, obj| memo += obj.ignored.size }
    end

    def failure_total
      @lang_results.reduce(0) { |memo, obj| memo += obj.failures.size }
    end

    def size
      @lang_results.reduce(0) { |memo, obj| memo += obj.size }
    end

    def to_s
      outfile = failed? ? "#{@filepath} has #{failure_total} errors (#{size} total keys checked, #{ignored_total} keys ignored)".red : "#{@filepath} passed (#{size} total keys checked, #{ignored_total} keys ignored)".green
      outfile = ignored? ? "#{@filepath} ignored".yellow : outfile
      interesting = @lang_results.select { |l| l.failed? || l.ignored? }
      "#{outfile}\n" + failures.map { |l| "  #{l}" }.join("\n")
    end
  end

  class LangResults
    def initialize(lang, entry_results)
      @lang          = lang
      @entry_results = entry_results
    end

    def ignored?
      false
    end

    def ignored
      @entry_results.select(&:ignored?)
    end

    def failed?
      @entry_results.any?(&:failed?)
    end

    def failures
      @entry_results.select(&:failed?)
    end

    def size
      @entry_results.size
    end

    def to_s
      to_str
    end

    def to_str(indent = 0)
      firstspacer = ' ' * indent
      spacer = ' ' * (indent + 2)
      failures = @entry_results.select(&:failed?)
      outlines = failures.map { |e| "#{spacer}#{e.to_str(indent + 2)}" }
      outlines = outlines.join("\n")
      outlang = failed? ? "#{@lang} has #{failures.size} keys with errors (#{size} keys checked, #{ignored.size} ignored)".red : "#{@lang} passed (#{size} keys checked, #{ignored.size} ignored)".green
      out = "#{firstspacer}#{outlang}\n#{outlines}"
    end
  end

  class MissingLangResult
    def initialize(lang)
      @lang = lang
    end

    def ignored?
      false
    end

    def ignored
      []
    end

    def failed?
      true
    end

    def failures
      []
    end

    def size
      0
    end

    def to_s
      to_str
    end

    def to_str(indent = 0)
      firstspacer = ' ' * indent
      "#{firstspacer}#{@lang} failed - missing #{@lang} dictionaries!".red
    end
  end

  class IgnoredLangResult
    def initialize(lang)
      @lang = lang
    end

    def ignored?
      true
    end

    def ignored
      []
    end

    def failed?
      false
    end

    def failures
      []
    end

    def size
      0
    end

    def to_s
      to_str
    end

    def to_str(indent = 0)
      firstspacer = ' ' * indent
      "#{firstspacer}#{@lang} ignored".yellow
    end
  end

  class EntryResults
    def initialize(key, word_results)
      @key          = key
      @word_results = word_results
    end

    def ignored?
      false
    end

    def failed?
      !@word_results.empty?
    end

    def to_s
      to_str
    end

    def to_str(indent = 0)
      spacer = ' ' * indent
      if failed?
        outlines = @word_results.map { |w| "#{spacer}#{w.to_str(indent + 2)}" }
        outlines = outlines.join("\n")
        "#{spacer}#{@key.red}:\n#{outlines}"
      else
        "#{spacer}#{@key} passed".green
      end
    end
  end

  class NullEntryResult
    def initialize(key)
      @key          = key
    end

    def ignored?
      false
    end

    def failed?
      true
    end

    def to_s
      to_str
    end

    def to_str(indent = 0)
      spacer = ' ' * indent
      "#{spacer}#{@key} is missing!".red
    end
  end

  class IgnoredEntryResult
    def initialize(key)
      @key = key
    end

    def ignored?
      true
    end

    def failed?
      false
    end

    def to_s
      to_str
    end

    def to_str(indent = 0)
      firstspacer = ' ' * indent
      "#{firstspacer}#{@key} ignored".yellow
    end
  end

  class MisspelledWordResult
    def initialize(word, suggestions)
      @word        = word
      @suggestions = suggestions
    end

    def failed?
      true
    end

    def to_s
      to_str
    end

    def to_str(indent = 0)
      spacer = ' ' * indent
      msg = "#{spacer}#{@word}".red

      if @suggestions && !@suggestions.empty?
        msg += " (#{@suggestions.map { |sug| sug.encode('utf-8') }.join(', ')})".yellow
      else
        msg += " (-- no suggestions --)".yellow
      end

      msg
    end
  end

  # Result capturing basic problems interacting with the file before parsing.
  class InvalidFilepathResult
    attr_reader :filepath, :error
    def initialize(filepath, error)
      @filepath = filepath
      @error    = error
    end

    def failed?
      true
    end

    def failure_total
      1
    end

    def to_s
      to_str
    end

    def to_str(indent = 0)
      spacer = ' ' * indent
      secondspacer = ' ' * (indent + 2)
      "#{spacer}#{@filepath} is invalid\n".red +
        "#{secondspacer}#{@error.message}".red
    end
  end

  # Result capturing problems parsing a readable file.
  class UnparseableFileResult
    attr_reader :filepath, :errors
    def initialize(filepath, errors)
      @filepath = filepath
      @errors   = Array(errors)
    end

    def failed?
      true
    end

    def failure_total
      errors.size
    end

    def to_s
      to_str
    end

    def to_str(indent = 0)
      spacer = ' ' * indent
      secondspacer = ' ' * (indent + 2)
      "#{spacer}#{@filepath} could not be parsed\n".red +
        @errors.map { |e| "#{secondspacer}#{e}".red }.join("\n")
    end
  end
end; end # Clausewitz::Spelling
